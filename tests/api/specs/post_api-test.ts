import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";

import {
  Post,
  PostsController,
  Comment,
} from "../lib/controllers/posts.controller";

let users;
const auth = new AuthController();

describe("Post controller", () => {
  let accessToken: string;
  const testRegister = global.appConfig.users.Alex;
  let posts;

  before("before", async () => {
    let response = await auth.login(testRegister.email, testRegister.password);
    console.log("response", response);
    accessToken = response.body.token.accessToken.token;
    users = new UsersController(accessToken);
    posts = new PostsController(accessToken);
  });

  after("after", async () => {
    await users.deleteUserById(testRegister.id);
  });

  it("should call getAllPosts with valid response", async () => {
    let response = await posts.getAllPosts();
    expect(response.statusCode, "Status code should be 200").to.be.equal(200);
    expect(
      response.timings.phases.total,
      "Response time should be less than 5s"
    ).to.be.lessThan(5000);
    expect(response.body.length).to.be.greaterThan(1);
  });

  const newPost: Post = {
    authorId: testRegister.id,
    previewImage: "some__image.png",
    body: "Test body for post",
  };

  it("should succesfully create new post with valid response", async () => {
    let response = await posts.createNewPost(newPost);
    newPost.id = response.body.id;
    expect(response.statusCode, "Status code should be 200").to.be.equal(200);
    expect(
      response.timings.phases.total,
      "Response time should be less than 5s"
    ).to.be.lessThan(5000);
  });

  const comment: Comment = {
    entityId: newPost.id || 10,
    isLike: true,
    userId: testRegister.id,
  };

  it("should succesfully create new comment on post with valid response", async () => {
    let response = await posts.addLikeToPost(comment);
    expect(response.statusCode, "Status code should be 200").to.be.equal(200);
    expect(
      response.timings.phases.total,
      "Response time should be less than 5s"
    ).to.be.lessThan(5000);
  });

  const invalidComment: Comment = {
    entityId: -1,
    isLike: true,
    userId: testRegister.id,
  };

  it("should fail on new comment on post with ivalid id", async () => {
    let response = await posts.addLikeToPost(invalidComment);
    expect(response.statusCode, "Status code should be 500").to.be.equal(500);
    expect(
      response.timings.phases.total,
      "Response time should be less than 5s"
    ).to.be.lessThan(5000);
  });

  const invalidUserIdComment: Comment = {
    entityId: newPost.id || 0,
    isLike: true,
    userId: -1,
  };

  it("should fail on new comment on post with invalid user id", async () => {
    let response = await posts.addLikeToPost(invalidUserIdComment);
    expect(response.statusCode, "Status code should be 500").to.be.equal(500);
    expect(
      response.timings.phases.total,
      "Response time should be less than 5s"
    ).to.be.lessThan(5000);
  });
});
