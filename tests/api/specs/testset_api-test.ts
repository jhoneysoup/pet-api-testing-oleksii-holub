import {
  checkResponseTime,
  checkStatusCode,
} from "../../helpers/functionsForChecking.helper";
import { AuthController } from "../lib/controllers/auth.controller";
const auth = new AuthController();

describe("Use test data set for login", () => {
  let invalidCredentialsDataSet = [
    { email: "golub541@gmail.com", password: "" },
    { email: "golub541@gmail.com", password: "   ____   " },
    { email: "golub541@gmail.com", password: "AMeGgapass120!" },
    { email: "golub541@gmail.com", password: "mega pass2021" },
    { email: "golub541@gmail.com", password: "admin" },
    { email: "golub541@gmail.com", password: "golub541@gmail.com" },
  ];

  invalidCredentialsDataSet.forEach((credentials) => {
    it(`should not login using invalid credentials : '${credentials.email}' + '${credentials.password}'`, async () => {
      let response = await auth.login(credentials.email, credentials.password);

      checkStatusCode(response, 401);
      checkResponseTime(response, 3000);
    });
  });
});
