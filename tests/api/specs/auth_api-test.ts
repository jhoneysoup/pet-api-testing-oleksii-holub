import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";

let users;
const auth = new AuthController();

describe("Auth controller", () => {
  let accessToken: string;
  const testRegister = global.appConfig.users.Alex;

  before("Delete user", async () => {
    try {
      let response = await auth.login(
        testRegister.email,
        testRegister.password
      );
      accessToken = response.body.token.accessToken.token;
      users = new UsersController(accessToken);
      await users.deleteUserById(testRegister.id);
    } catch (err) {
      console.error(err);
    }
  });

  it("Register successful", async () => {
    let response = await auth.register(
      testRegister.id,
      testRegister.avatar,
      testRegister.email,
      testRegister.userName,
      testRegister.password
    );
    expect(response.statusCode, "Status code should be 201").to.be.equal(201);
    expect(
      response.timings.phases.total,
      "Response time should be less than 5s"
    ).to.be.lessThan(5000);
  });

  it(`Login and update token successful`, async () => {
    let response = await auth.login(testRegister.email, testRegister.password);
    expect(response.statusCode, "Status code should be 200").to.be.equal(200);
    expect(
      response.timings.phases.total,
      "Response time should be less than 5s"
    ).to.be.lessThan(5000);
  });
});
