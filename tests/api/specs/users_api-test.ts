import { expect, use } from "chai";
import { User, UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import {
  checkStatusCode,
  checkResponseTime,
} from "../../helpers/functionsForChecking.helper";

let users;
const auth = new AuthController();

describe("Users controller", () => {
  let accessToken: string;
  const testRegister = {
    id: 3319,
    email: "mail@mail.com",
    password: "pass",
    userName: "username",
    avatar: "avatar",
  };

  before("before", async () => {
    await auth.register(
      testRegister.id,
      testRegister.avatar,
      testRegister.email,
      testRegister.userName,
      testRegister.password
    );
    let response = await auth.login(testRegister.email, testRegister.password);
    accessToken = response.body.token.accessToken.token;
    users = new UsersController(accessToken);
  });

  it("should call getAllUsers with valid response", async () => {
    let response = await users.getAllUsers();
    expect(response.statusCode, "Status code should be 200").to.be.equal(200);
    expect(
      response.timings.phases.total,
      "Response time should be less than 5s"
    ).to.be.lessThan(5000);
    expect(response.body.length).to.be.greaterThan(1);
  });

  it("should revoke user from access token", async () => {
    const response = await users.fromToken(accessToken);
    const { avatar, email, id, userName } = testRegister;
    expect(response.statusCode, "Status code should be 200").to.be.equal(200);
    expect(
      response.timings.phases.total,
      "Response time should be less than 5s"
    ).to.be.lessThan(5000);

    expect(response.body).to.eql({
      avatar,
      email,
      id,
      userName,
    });
  });

  const newUser: User = {
    id: testRegister.id,
    avatar: "new_value",
    email: "new@mail.com",
    userName: "new_username",
  };
  it("should update user", async () => {
    const response = await users.update(newUser);
    expect(response.statusCode, "Status code should be 204").to.be.equal(204);
    expect(
      response.timings.phases.total,
      "Response time should be less than 5s"
    ).to.be.lessThan(5000);
  });

  it(`should return user details when getting user details with valid id`, async () => {
    let response = await users.getAllUsers();
    let firstUserId: number = response.body[0].id;

    response = await users.getUserById(firstUserId);

    checkStatusCode(response, 200);
    checkResponseTime(response, 1000);
  });

  it(`should delete user with valid id`, async () => {
    let response = await users.deleteUserById(testRegister.id);
    checkStatusCode(response, 204);
    checkResponseTime(response, 1000);
  });
});
