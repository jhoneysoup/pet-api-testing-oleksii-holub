import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl;

export type Post = {
  id?: number;
  authorId: number;
  previewImage: string;
  body: string;
};

export type Comment = {
  entityId: number;
  isLike: boolean;
  userId: number;
};

export class PostsController {
  public accessToken?: string;

  constructor(accessToken?: string) {
    this.accessToken = accessToken;
  }

  async createNewPost({ authorId, previewImage, body }: Post) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("POST")
      .bearerToken(this.accessToken)
      .url(`api/Posts`)
      .body({
        authorId,
        previewImage,
        body,
      })
      .send();
    return response;
  }

  async addLikeToPost({ entityId, isLike, userId }: Comment) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("POST")
      .url(`api/Posts/like`)
      .bearerToken(this.accessToken)
      .body({
        entityId,
        isLike,
        userId,
      })
      .send();
    return response;
  }

  async getAllPosts() {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("GET")
      .url(`api/Posts`)
      .send();
    return response;
  }
}
