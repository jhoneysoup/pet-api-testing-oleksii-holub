import { ApiRequest } from "../request";

export class AuthController {
  async login(emailValue: string, passwordValue: string) {
    const response = await new ApiRequest()
      .prefixUrl("http://tasque.lol/")
      .method("POST")
      .url(`api/Auth/login`)
      .body({
        email: emailValue,
        password: passwordValue,
      })
      .send();
    return response;
  }

  async register(
    id: number,
    avatar: string,
    email: string,
    userName: string,
    password: string
  ) {
    const response = await new ApiRequest()
      .prefixUrl("http://tasque.lol/")
      .method("POST")
      .url(`api/Register`)
      .body({
        id,
        avatar,
        email,
        userName,
        password,
      })
      .send();
    return response;
  }
}
