import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl;

export type User = {
  email: string;
  password?: string;
  id?: number;
  avatar: string;
  userName: string;
};

export class UsersController {
  public accessToken?: string;

  constructor(accessToken?: string) {
    this.accessToken = accessToken;
  }

  async getAllUsers() {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("GET")
      .url(`api/Users`)
      .send();
    return response;
  }

  async getUserById(id) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("GET")
      .url(`api/Users/${id}`)
      .send();
    return response;
  }

  async getCurrentUser(accessToken: string) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("GET")
      .url(`api/Users/fromToken`)
      .bearerToken(accessToken)
      .send();
    return response;
  }

  async updateUser(userData: object, accessToken: string) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("PUT")
      .url(`api/Users`)
      .body(userData)
      .bearerToken(accessToken)
      .send();
    return response;
  }

  async deleteUserById(id) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .bearerToken(this.accessToken)
      .method("DELETE")
      .url(`api/Users/${id}`)
      .send();
    return response;
  }

  async fromToken(token?: string) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .bearerToken(token)
      .method("GET")
      .url("api/Users/fromToken")
      .send();
    return response;
  }

  async update(user: User) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .bearerToken(this.accessToken)
      .method("PUT")
      .url("api/Users")
      .body(user)
      .send();
    return response;
  }
}
